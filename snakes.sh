#!/bin/bash

LINES=$(tput lines)
COLUMNS=$(tput cols)

EXEC=/tmp/s

# /proc/bus/input/devices 


rm -rf $EXEC \
    && gcc -Wall -Wextra -O2 -g snakes.c -o $EXEC \
    && $EXEC $LINES $COLUMNS /dev/input/event7
