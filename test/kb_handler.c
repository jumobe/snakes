/**
 *
 *  /dev/input/event7
 * /dev/input/event14
 * cat /proc/bus/input/devices | grep -i keyboard
 */



#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  int fd;
  if(argc < 2)
    {
      printf("usage: %s <device>\n", argv[0]);
      return 1;
    }

  fd = open(argv[1], O_RDONLY);
  struct input_event ev;

  printf("*\n");
  int type = 0;
  while (1)
    {
      read(fd, &ev, sizeof(struct input_event));
      if ( ev.type != type )
        {
          printf("key=[%i] state=[%i] type=[%i]\n", ev.code, ev.value, ev.type);
          type = ev.type;
        }
    }
}
