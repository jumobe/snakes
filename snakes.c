/* -*- mode:c;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*- */

#include <fcntl.h>
#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

typedef enum
{
  UNKNOWN = 102,
  UP = 103,
  DOWN = 108 ,
  LEFT = 105,
  RIGHT = 106
} direction;

typedef struct
{
  unsigned short x;
  unsigned short y;
  direction d;
} snake_element;

unsigned short rand_pos(unsigned short ceiling)
{
  return (rand() % (ceiling/2)) + (ceiling/3);
}

void clear_display()
{
  /* system("clear"); */
  printf ( "\033[2J");  
}

void print_display(unsigned short lines,
                   unsigned short cols,
                   snake_element *se,
                   unsigned short sz_snake,
                   unsigned short x_egg,
                   unsigned short y_egg)
{
  unsigned short i;
  clear_display();
  for(i = 0; i < sz_snake; i++)
  {
    /* move to x,y */
    printf ( "\033[%d;%dH", se[i].y, se[i].x); 
    printf("%c", '*');
  }
  printf ( "\033[%d;%dH", y_egg, x_egg);
  printf("%c", 'o');
  printf ( "\033[%d;%dH", lines, cols);
  printf("%c\n", ' ');
  fflush(stdout);
}

snake_element *start_snake(unsigned short lines,
                           unsigned short cols)
{
  snake_element *se =
    (snake_element*)malloc(sizeof(snake_element));
  se->x = rand_pos(cols);
  se->y = rand_pos(lines);
  se->d = RIGHT;
  return se;
}

void update_snake_element(snake_element *se, direction d)
{
  switch(d)
  {
  case UP:
  {
    se->y -= 1;
    break;
  }
  case DOWN:
  {
    se->y += 1;
    break;
  }
  case LEFT:
  {
    se->x -= 1;
    break;
  }
  case RIGHT:
  {
    se->x += 1;
    break;
  }
  case UNKNOWN:
    break;
  }
}

void grow_snake(snake_element **se,
                unsigned short *sz_snake)
{
  unsigned short nsz = *sz_snake + 1;
  *se = (snake_element *)
    realloc(*se, nsz * sizeof(snake_element));
    
  (*se)[nsz-1] = (*se)[nsz-2];

  switch((*se)[nsz-1].d)
  {
  case DOWN:
  {
    (*se)[nsz-1].y = (*se)[nsz-1].y - 1;
    break;
  }
  case UP:
  {
    (*se)[nsz-1].y = (*se)[nsz-1].y + 1;
    break;
  }
  case LEFT:
  {
    (*se)[nsz-1].x = (*se)[nsz-1].x + 1;
    break;
  }
  case RIGHT:
  {
    (*se)[nsz-1].x = (*se)[nsz-1].x - 1;
    break;
  }
  default:
    break;
  }
  
  *sz_snake = nsz;
}

int move_snake(unsigned short lines,
               unsigned short cols,
               snake_element **se,
               unsigned short *sz_snake,
               direction d,
               unsigned short *x_egg,
               unsigned short *y_egg)
{
  unsigned short i;
  direction move_direction = (d == UNKNOWN) ? (*se)[0].d : d;
  
  for(i = 0; i < *sz_snake; i++)
  {
    update_snake_element(&(*se)[i], move_direction);
    
    direction tmp_direction = (*se)[i].d;
    (*se)[i].d = move_direction;
    move_direction = tmp_direction;   

    if(((*se)[i].y == (lines-1)) || ((*se)[i].y == 0)
       || ((*se)[i].x == (cols-1)) || ((*se)[i].x == 0))
    {
      return 0;
    }
  }

  /* egg */
  if(((*se)[0].y == *y_egg) && ((*se)[0].x == *x_egg))
  {
    *y_egg = rand_pos(lines);
    *x_egg = rand_pos(cols);
    grow_snake(se, sz_snake);
  }  
  
  return 1;
}

direction read_kb(int fd)
{  
  struct input_event ev;  
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec = 100;  
  fd_set set;  
  FD_ZERO(&set); 
  FD_SET(fd, &set); 

  if ( select(fd + 1, &set, NULL, NULL, &timeout) > 0 )
  {
    if( read(fd, &ev, sizeof(struct input_event)) >= 0)
    {
      if ( ( ev.type == 1 ) &&         
           ((ev.code == UP) || (ev.code == DOWN)
            || (ev.code == LEFT) || (ev.code == RIGHT)) )
      {
        return ev.code;
      }
    }
  }
  return UNKNOWN;
}

int main(int argc, char **argv)
{
  if(argc < 4)
  {
    fprintf(stderr,
            "Usage: %s <n-lines> <n-columns> /dev/input/eventX\n",
            argv[0]);
    return 1;
  }
  
  unsigned short lines = atoi(argv[1]) - 1;
  unsigned short columns = atoi(argv[2]);
  unsigned short x_egg = rand_pos(columns),
    y_egg = rand_pos(lines);

  snake_element *se = start_snake(lines, columns);
  unsigned short sz_snake = 1;
  int fd = open(argv[3], O_RDONLY);
  
  int loop = 1;
  while(loop)
  {
    loop = move_snake(lines, columns, &se, &sz_snake,
                      read_kb(fd), &x_egg, &y_egg);
    print_display(lines, columns, se, sz_snake,
                  x_egg, y_egg);
    usleep(70000 - (sz_snake * 2000));
  }
  free(se);
  close(fd);
  return 0;
}
